package pl.edu.pwsztar.domain.files.filesImpl;

import pl.edu.pwsztar.domain.files.TxtGenerator;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FileDto;


import java.io.*;


@Service

public class TxtGeneratorImpl implements TxtGenerator {


    private final static String PREFIX = "tmp";
    private final static String SUFFIX = ".txt";

    @Override
    public File toTxt (FileDto fileDto) throws IOException{
        File file = getGeneratedFile(fileDto);
        return file;
    }



    private File getGeneratedFile(FileDto fileDto) throws IOException {
        File file = File.createTempFile(PREFIX,SUFFIX);
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(fileOutputStream));
        writeToFile(bufferedWriter,fileDto);
        bufferedWriter.close();
        fileOutputStream.flush();
        fileOutputStream.close();
        return file;
    }



    private void writeToFile(BufferedWriter bufferedWriter,FileDto fileDto) throws IOException {
        fileDto.getMovieList().
                stream().
                sorted((movie1, movie2) -> (movie1.getYear()-movie2.getYear())*-1).
                forEach(movie->{
                            try {
                                bufferedWriter.write(movie.getYear()+ " "+ movie.getTitle());
                                bufferedWriter.newLine();
                            } catch (IOException e) {
                            }
    });
    }

}
